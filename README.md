# Appico Dashboard UI by Urko Pineda Olmo

This is the **Dashboard UI practice by me, Urko Pineda Olmo.** This practice has been developed entirely in **Next.js.**

## Features

- ⚛⚛ Web app developed in React.js -> Next.js.
- 💯/💯/💯/💯 in Lighthouse.
- 🖥 Server Side Rendering (SSR).
- 📱 Progresive Web App (PWA).
- 🌙 Dark mode (*).
- ✅ Web app deployed in `now.sh` by ZEIT in [https://appico.now.sh](https://appico.now.sh).
- ✅ API endpoint added for fetching data.
- ✅ Icons added using SVG sprites.

![](lighthouse.png)

> (*) Turn dark mode in your OS to check it out!

## Project structure

The project is structured in the next directories:
- In **`components` directory** are located **all the components used in other components and pages.** This components are structured using an **atomic design,** where components like **`atoms`** are the lighter components and **`molecules` and `organisms`** are more complex components.
- In **`data` directory** are located **static data** (like `menu.js` for all menu data) used in the web app.
- In **`pages` directory** are localted **all pages used by the route system**. For this practice only `index.js` page has been developed. Other dummy pages have been added to demostrate the routing system.
- In **`pages/api` directory** are localted **API endpoints of the web app**. For this practice only `api/overview.js` endpoint has been developed.
- In **`public` directory** are located **all the static assets** of the web app like the site manifest, images and more.

There are other important files too in the project:
- `gulpfile.js` defines **all the tasks needed to optimize and transforms icons to an SVG sprite** to use it in the web app.
- `next.config.js` file adds some **custom options** (like `next-offline` for PWA set up) to Next.js.
- `now.json` defines some **custom options for deployment** in ZEIT's `now.sh` platform.

## Scripts

- `yarn start` to start a development server.
- `yarn build` to build a produdction server.
- `yarn serve` to start serving the server localy.
- `yarn generate-icons` to optimize and transform `public/icons/*.svg` into a SVG sprite in `public/icons.svg` using Gulp.
