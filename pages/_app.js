import React from 'react'

import Layout from '../components/Layout'

/**
 * This is the `_app.js` file, an internal page of Next.js.
 * The purpouse of this file is to add a layout to the web app through the `Layout` component.
 * This layout includes global HTML structure and global CSS.
 * @param {node}   Component or page to be rendered
 * @param {object} pageProps of the component / page
 */
const MyApp = ({ Component, pageProps }) => (
  <Layout>
    <Component
      {...pageProps}
    />
  </Layout>
)

export default MyApp
