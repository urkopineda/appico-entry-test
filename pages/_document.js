import React from 'react'
import Document, {
  Html,
  Head,
  Main,
  NextScript,
} from 'next/document'

/**
 * This is the `_document.js` file, an internal page of Next.js.
 * We only need it to add the `lang` property to the `html` tag which improves the SEO results.
 * This tag should change when the user changes the language (future improvement).
 */
class MyDocument extends Document {
  render = () => {
    return (
      <Html lang="en">
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

MyDocument.getInitialProps = (context) => (
  Document.getInitialProps(context)
)

export default MyDocument
