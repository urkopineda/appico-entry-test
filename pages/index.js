import React, { useMemo } from 'react'
import fetch from 'isomorphic-unfetch'

import Paper from '../components/atoms/Paper'
import LineChart from '../components/molecules/LineChart'
import BarChart from '../components/molecules/BarChart'
import PieChart from '../components/molecules/PieChart'
import Table from '../components/molecules/Table'

/**
 * Parse the bar chart data from an object to an array
 * @param   {object} data to be pared
 * @returns {array}  data parsed
 */
const parseBarChartData = (data) => {
  const result = []
  for (let key in data) {
    result.push({
      label: key,
      progress: data[key],
    })
  }
  return result
}

/**
 * Render the results using some values, a bar chart and a pie chart.
 * @param {string} name to be shown as a title.
 * @param {object} results to be shown on graphs.
 * @param {object} borders props passed to `Paper` component.
 * @param {number} accentColor number to change the color of the results.
 */
const Results = ({ name, results, borders, accentColor }) => {
  // Create a memo of the progress to avoid recalculating.
  const progress = useMemo(
    () => (results.followers.count / results.followers.total) * 100,
    [ results ]
  )
  // Create a memo of the parsed data to avoid recalculating.
  const parsedData = useMemo(
    () => parseBarChartData(results.data),
    [ results ]
  )

  return (
    <div className="container">
      <Paper
        borders={borders}
        bradius={false}
      >
        <div className="subcontainer">
          <div className="left-container">
            <h2>
              {name}
            </h2>
            <p>
              <span>{(results.followers.total).toLocaleString('en')}</span> Followers
            </p>
            <BarChart
              data={parsedData}
              accentColor={accentColor}
            />
          </div>
          <div className="right-container">
            <PieChart
              progress={progress}
              accentColor={accentColor}
            />
          </div>
        </div>
      </Paper>
      <style jsx>
        {`
          .container {
            flex: 2;
            min-width: 250px;
          }
  
          .subcontainer {
            display: flex;
            flex-direction: row;
            justify-content: center;
            flex-wrap: wrap;
          }
  
          .left-container {
            flex: 1;
            display: flex;
            flex-direction: column;
            align-items: center;
            min-width: 180px;
          }
  
          .left-container > h2 {
            width: 100%;
            margin: 0px 0px 30px 0px;
            font-size: 17px;
          }
  
          .left-container > p {
            width: 100%;
            color: var(--appico-text-disabled-color);
          }
  
          .left-container > p > span {
            margin: 0px 10px 0px 0px;
            color: var(--appico-font-color);
            font-weight: bold;
            font-size: 30px;
          }
  
          .right-container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            padding: 30px 10px 10px 10px;
          }
        `}
      </style>
    </div>
  )
}

/**
 * This is the main page of the web app.
 * Through props we provide the data obtained from `api/overview.js`.
 * @param {object} props with `data` property.
 */
const Overview = (props) => {
  const {
    data,
  } = props

  // Extract data usign object destructuring.
  const {
    insertions,
    general,
    ratings,
    terms,
    supportRequests,
  } = data

  // Create a memo of the progress to avoid recalculating.
  const insertionProgress = useMemo(
    () => Math.round((insertions.pending / insertions.total) * 10000) / 100,
    [ insertions ]
  )

  return (
    <>
      <div className="header-container">
        <h1>
          Data overview
        </h1>
        <div className="header-chart-container">
          <LineChart
            progress={insertionProgress}
            maxWidth={200}
          />
          <p>
            <span>{insertions.pending}</span> insertions needed to complete
          </p>
        </div>
      </div>
      <Paper
        padding={false}
        borders={{
          top: true,
          left: true,
        }}
      >
        <div className="paper-header-container">
          <Results
            name="General results"
            results={general}
            borders={{
              right: true,
              bottom: true,
            }}
            accentColor={1}
          />
          <Results
            name="Ratings by category"
            results={ratings}
            borders={{
              right: true,
              bottom: true,
            }}
            accentColor={2}
          />
          <div className="terms-container">
            {Object.keys(terms).map((key) => (
              <div
                key={key}
                className="term-container"
              >
                <span>
                  Term {key}
                </span>
                <span>
                  {(Math.round(terms[key]) / 10).toLocaleString('en')}
                </span>
              </div>
            ))}
          </div>
        </div>
      </Paper>
      <Table
        title="Support Requests"
        borders={{
          bottom: true,
          left: true,
          right: true,
        }}
        data={supportRequests}
        action={{
          key: 'status',
          active: 'Sent',
        }}
      />
      <style jsx>
        {`
          .header-container {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            align-items: center;
            margin: 0px 0px 10px 0px;
          }

          .header-container > h1 {
            margin: 0px auto 0px 0px;
          }

          .header-chart-container {
            flex: 1;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            align-items: center;
            justify-content: flex-end;
            min-width: 180px;
          }

          .header-chart-container > p {
            margin: 0px 0px 0px 10px;
            text-align: right;
            color: var(--appico-text-disabled-lighter-color);
          }

          .header-chart-container > p > span {
            color: var(--appico-font-color);
            font-weight: bold;
          }

          .paper-header-container {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
          }

          .terms-container {
            flex: 1;
            min-width: 200px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            align-items: center;
            padding: 20px 10px;
            background-color: var(--appico-paper-color);
            border-width: 1px;
            border-color: var(--appico-divider-color);
            border-right-style: solid;
            border-bottom-style: solid;
            border-radius: 0px 3.8px 3.8px 0px;
            overflow: hidden;
          }

          .term-container {
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: space-around;
            align-items: center;
          }

          .term-container span {
            font-size: 13px;
          }

          .term-container span:first-child {
            text-transform: uppercase;
            color: var(--appico-text-disabled-color);
          }
        `}
      </style>
    </>
  )
}

/**
 * This part would be executed on server side, that's why we need to add explicitly the URI of the server.
 * We will obtain the randomized data from the API endpoint `api/overview.js`.
 */
Overview.getInitialProps = async () => {
  let url = 'http://localhost:3000'
  if (process.env.NODE_ENV === 'production') {
    url = 'https://appico.now.sh'
  }
  const result = await fetch(`${url}/api/overview`)
  const data = await result.json()
  return {
    data,
  }
}

export default Overview
