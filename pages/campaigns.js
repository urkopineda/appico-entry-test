import React from 'react'

/**
 * This is only a dummy page to test the routing system.
 */
const Dummy = () => (
  <div />
)

export default Dummy
