const generateRandomNumber = (max) => Math.floor((Math.random() * max) + 1)

const DATA = [
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'o',
  'l',
  'm',
  'n',
  'o',
]

const NAMES = [
  'Urko Pineda',
  'Cecilia Welch',
  'Sara Glover',
  'Harriett Morgan',
  'Susie Curry',
  'Edgar Greer',
  'Minerva Massey',
]

const EMAILS = [
  'heather_keeling@gottlieb.ca',
  'floyd_brakus@lindgre',
  'jabari.kilback@nelso',
  'theo_gleichner@kaia.org',
  'ankunding_ralph@gmail.com',
  'lia_purdy@yahoo.com',
]

const PHONE_NUMBERS = [
  '215-593-5846',
  '057-812-3947',
  '866-668-0327',
  '647-851-5280',
  '985-747-0063',
  '488-514-5012',
]

const CITIES = [
  'South Mariane',
  'East Maryam',
  'Monserratmouth',
  'Lonnyburgh',
  'Schmittfurt',
  'South Lori',
]

/**
 * Generate random data for overview page
 */
export default (request, response) => {
  const data = {}
  // Add insertions: total and pending
  data.insertions = {
    total: generateRandomNumber(1000),
  }
  data.insertions.pending = generateRandomNumber(data.insertions.total)
  // Add general: followers (total and count) and data (a ... o with 0 to 100)
  data.general = {
    followers: {
      total: generateRandomNumber(10000),
    },
    data: {

    }
  }
  data.general.followers.count = generateRandomNumber(data.general.followers.total)
  for (let d of DATA) {
    data.general.data[d] = generateRandomNumber(100)
  }
  // Add ratings: followers (total and count) and data (a ... o with 0 to 100)
  data.ratings = {
    followers: {
      total: generateRandomNumber(10000),
    },
    data: {

    }
  }
  data.ratings.followers.count = generateRandomNumber(data.ratings.followers.total)
  for (let d of DATA) {
    data.ratings.data[d] = generateRandomNumber(100)
  }
  // Add terms 1 to 5
  data.terms = {}
  for (let i = 0; i < 4; i++) {
    data.terms[i + 1] = generateRandomNumber(1000)
  }
  // Add support requests
  data.supportRequests = {
    headers: {
      name: 'Name',
      email: 'Email',
      time: 'Time',
      phoneNumber: 'Phone number',
      city: 'City',
      status: 'Status',
    },
    data: [],
  }
  for (let i = 0; i < 25; i++) {
    const date = new Date()
    date.setSeconds(generateRandomNumber(24 /* hours */ *  60 /* minutes */ * 60 /* seconds */))
    data.supportRequests.data.push({
      name: NAMES[generateRandomNumber(NAMES.length - 1)],
      email: EMAILS[generateRandomNumber(EMAILS.length - 1)],
      time: date.toLocaleString('en', {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true,
      }),
      phoneNumber: PHONE_NUMBERS[generateRandomNumber(PHONE_NUMBERS.length - 1)],
      city: CITIES[generateRandomNumber(CITIES.length - 1)],
      status: (generateRandomNumber(2) === 1) ? 'Sent' : 'Open',
    })
  }
  // Return the generated data
  response.statusCode = 200
  response.setHeader('Content-Type', 'application/json')
  response.end(JSON.stringify(data))
}
