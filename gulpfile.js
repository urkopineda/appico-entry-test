const gulp = require('gulp')
const svgs = require('gulp-svg-sprite')
const svgo = require('gulp-svgo')
const clean = require('gulp-clean')
const rename = require('gulp-rename')

// Clean / remove current sprite.
gulp.task('svg-sprite-clean-start', () =>
  gulp.src('public/icons.svg', { read: false, allowEmpty: true })
    .pipe(clean())
)
// Optimize all SVGs and move them to `out/`.
gulp.task('svg-sprite-optimize', () =>
  gulp.src('public/icons/**/*.svg')
    .pipe(svgo())
    .pipe(gulp.dest('out/svgo'))
)
// Generate the sprite based on the optimized SVGs.
gulp.task('svg-sprite-generate', () =>
  gulp.src('out/svgo/**/*.svg')
    .pipe(svgs({
      mode: {
        symbol: true,
      },
    }))
    .pipe(gulp.dest('out/svgs'))
)
// Move the sprite to `public/icons.svg`.
gulp.task('svg-sprite-move', () =>
  gulp.src('out/svgs/symbol/svg/sprite.symbol.svg')
    .pipe(rename('icons.svg'))
    .pipe(gulp.dest('public'))
)
// Clean `out/` directory.
gulp.task('svg-sprite-clean-end', () =>
  gulp.src('out', { read: false, allowEmpty: true })
    .pipe(clean())
)

gulp.task('svg-sprite', gulp.series(
  'svg-sprite-clean-start',
  'svg-sprite-optimize',
  'svg-sprite-generate',
  'svg-sprite-move',
  'svg-sprite-clean-end'
))
