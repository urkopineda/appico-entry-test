/**
 * This is the menu data, adding or removing objects to this array will change the shape
 * of the top and side menu.
 */
export default [
  {
    key: 'overview',
    href: '/',
    text: 'Overview',
    submenu: [
      {
        key: 'index',
        href: {
          pathname: '/',
        },
        icon: 'tachometer',
      },
      {
        key: 'messages',
        href: {
          pathname: '/',
          query: {
            section: 'messages',
          },
        },
        icon: 'comments',
      },
      {
        key: 'folders',
        href: {
          pathname: '/',
          query: {
            section: 'folders',
          },
        },
        icon: 'folder-open',
      },
      {
        key: 'mails',
        href: {
          pathname: '/',
          query: {
            section: 'mails',
          },
        },
        icon: 'envelope',
      },
      {
        key: 'settings',
        href: {
          pathname: '/',
          query: {
            section: 'settings',
          },
        },
        icon: 'cog',
      },
    ],
  },
  {
    key: 'campaigns',
    href: '/campaigns',
    text: 'Campaigns',
    submenu: [
      {
        key: 'index',
        href: {
          pathname: '/campaigns',
        },
        icon: 'tachometer',
      },
      {
        key: 'messages',
        href: {
          pathname: '/campaigns',
          query: {
            section: 'messages',
          },
        },
        icon: 'comments',
      },
      {
        key: 'settings',
        href: {
          pathname: '/campaigns',
          query: {
            section: 'settings',
          },
        },
        icon: 'cog',
      },
    ],
  },
  {
    key: 'analytics',
    href: '/analytics',
    text: 'Analytics',
    submenu: [
      {
        key: 'index',
        href: {
          pathname: '/analytics',
        },
        icon: 'tachometer',
      },
      {
        key: 'mails',
        href: {
          pathname: '/analytics',
          query: {
            section: 'mails',
          },
        },
        icon: 'envelope',
      },
      {
        key: 'settings',
        href: {
          pathname: '/analytics',
          query: {
            section: 'settings',
          },
        },
        icon: 'cog',
      },
    ],
  },
]
