import React from 'react'
import PropTypes from 'proptypes'

/**
 * This component renders a line chart.
 * @param {object} props check the props doc in the PropTypes definition bellow.
 */
const LineChart = ({ progress, maxWidth, minWidth, accentColor }) => (
  <>
    <div
      className="container"
      style={{
        '--progress': `${progress}%`,
        '--max-width': `${maxWidth}px`,
        '--min-width': `${minWidth}px`,
        '--background-color': `var(--appico-accent-main-color-${accentColor})`,
      }}
    >
      <div className="progress" />
    </div>
    <style jsx>
      {`
        @keyframes load-animation {
          from {
            max-width: 0px;
          }
        }

        .container {
          width: 100%;
          height: 5px;
          border-radius: 2.5px;
          background-color: var(--appico-linechart-color);
          max-width: var(--max-width);
          min-width: var(--min-width);
          margin: 11px 5px 10px 5px;
        }

        .progress {
          width: 100%;
          height: 100%;
          border-radius: 2.5px;
          background-color: var(--background-color);
          max-width: var(--progress);
          animation: load-animation 1.5s ease forwards;
          animation-delay: 250ms;
        }
      `}
    </style>
  </>
)

LineChart.defaultProps = {
  progress: 10,
  maxWidth: 100,
  minWidth: 75,
  accentColor: 1,
}

LineChart.propTypes = {
  /** Progress percentage, from 0 to 100 */
  progress: PropTypes.number,
  /** Max width of the graph */
  maxWidth: PropTypes.number,
  /** Min width of the graph */
  minWidth: PropTypes.number,
  /** Accent color code, 1 or 2 */
  accentColor: PropTypes.oneOf([1, 2]),
}

export default LineChart
