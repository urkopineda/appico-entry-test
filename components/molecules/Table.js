import React, { useState, useMemo } from 'react'
import PropTypes from 'proptypes'

import Paper from '../atoms/Paper'
import Button from '../atoms/Button'

/**
 * This component renders a table.
 * @param {object} props check the props doc in the PropTypes definition bellow.
 */
const Table = ({ title, borders, max, data: tableData, action }) => {
  const {
    headers,
    data,
  } = tableData

  // Page state of the table
  const [ page, setPage ] = useState(0)
  // Count of the data available
  const length = data.length
  // Start value of the pagination, we use a memo to avoid recalculations.
  const start = useMemo(
    () => (page * max),
    [ page, max ]
  )
  // End value of the pagination, we use a memo to avoid recalculations.
  const end = useMemo(
    () => ((page + 1) * max),
    [ page, max ]
  )
  // Number of pages available, we use a memo to avoid recalculations.
  const pages = useMemo(
    () => Math.ceil(length / max),
    [ length, max ]
  )
  // Paginated data shown, we use a memo to avoid recalculations.
  const paginatedData = useMemo(
    () => data.slice(start, end),
    [ data, start, end ]
  )

  return (
    <>
      <Paper borders={borders}>
        <h2>
          {title}
        </h2>
        <div className="table">
          <div className="table-header">
            {Object.keys(headers).map((key) => (
              <span key={key}>
                {headers[key]}
              </span>
            ))}
          </div>
          <div className="table-data">
            {paginatedData.map((d, i) => (
              <div
                key={i}
                className="table-data-element"
              >
                {Object.keys(headers).map((key) => (
                  <span key={key}>
                    {(action && (action.key === key))
                      ? (
                        <Button
                          disabled={(action && action.active === d[key])}
                        >
                          {d[key]}
                        </Button>
                      )
                      : d[key]
                    }
                  </span>
                ))}
              </div>
            ))}
          </div>
        </div>
      </Paper>
      <div className="pagination-container">
        <div className="pagination-counter-container">
          Showing <span>{start + 1}</span> to <span>{(end > length) ? length : end}</span> of {length} elements
        </div>
        <div className="pagination-pages-container">
          {[...new Array(pages)].map((nothing, i) => (
            <div key={i}>
              <Button
                disabled={!(page === i)}
                onClick={() => setPage(i)}
              >
                {i + 1}
              </Button>
            </div>
          ))}
        </div>
      </div>
      <style jsx>
        {`
          .table {
            width: 100%;
            margin: 30px 0px 0px 0px;
            display: flex;
            flex-direction: column;
            overflow-x: scroll;
          }

          .table-header {
            margin: 0px 0px 10px 0px;
            display: flex;
            flex-direction: row;
          }

          .table-data {
            flex: 1;
            display: flex;
            flex-direction: column;
          }

          .table-data-element {
            margin: 10px 0px;
            flex: 1;
            display: flex;
            flex-direction: row;
            align-items: center;
          }

          .table span {
            padding: 0px 20px 0px 0px;
            flex: 1;
            min-width: 200px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
          }

          .table-header > span {
            text-transform: uppercase;
            font-size: 13px;
            color: var(--appico-text-disabled-lighter-color);
          }

          .table-data-element > span:not(:first-child) {
            color: var(--appico-text-disabled-lighter-color);
          }

          .pagination-container {
            margin: 30px 0px 0px 0px;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            align-items: center;
            justify-content: space-between;
          }

          .pagination-counter-container {
            margin: 0px 0px 10px 0px;
            min-width: 210px;
            color: var(--appico-text-disabled-color);
          }

          .pagination-counter-container > span {
            color: var(--appico-font-color);
          }

          .pagination-pages-container {
            display: flex;
            flex-direction: row;
            align-items: center;
            overflow-x: scroll;
          }

          .pagination-pages-container > div {
            margin: 0px 5px 0px 0px;
          }
        `}
      </style>
    </>
  )
}

Table.defaultProps = {
  title: 'Default title',
  borders: {
    top: true,
    bottom: true,
    left: true,
    right: true,
  },
  max: 6,
  data: {
    headers: {
      test: 'Column name',
    },
    data: [
      {
        test: 'Data 1'
      },
      {
        test: 'Data 2'
      },
      {
        test: 'Data 3'
      },
    ],
  },
}

Table.propTypes = {
  /** Title of the table */
  title: PropTypes.string,
  /** Borders set up (top, bottom, left, right) */
  borders: PropTypes.shape({
    top: PropTypes.bool,
    bottom: PropTypes.bool,
    left: PropTypes.bool,
    right: PropTypes.bool,
  }),
  /** Maximum lines per page */
  max: PropTypes.number,
  /** Data with headers */
  data: PropTypes.shape({
    headers: PropTypes.objectOf(
      PropTypes.string
    ),
    data: PropTypes.arrayOf(
      PropTypes.objectOf(
        PropTypes.any
      )
    ),
  }),
  /** Action for the table */
  action: PropTypes.shape({
    /** Key of the action data */
    key: PropTypes.string,
    /** Active action vale */
    active: PropTypes.string,
  }),
}

export default Table
