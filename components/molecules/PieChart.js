import React, { useMemo } from 'react'
import PropTypes from 'proptypes'

/**
 * This component renders a pie chart.
 * The inspiration of this code is pie charts that Lighthouse uses for displaying the
 * ranking of a page (which in our case is fenomenal 😎).
 * @param {object} props check the props doc in the PropTypes definition bellow.
 */
const PieChart = ({ progress, size: s, margin, stroke, accentColor }) => {
  // Size of the full chart, we use a memo to avoid recalculations.
  const size = useMemo(
    () => (s + margin * 2),
    [ s, margin ]
  )
  // Full circunference size, we use a memo to avoid recalculations.
  const circ = useMemo(
    () => (s * Math.PI + (stroke * 3)),
    [ s, stroke ]
  )
  // Radius of the circle, we use a memo to avoid recalculations.
  const radius = useMemo(
    () => (s / 2),
    [ s ]
  )
  // Value of the chart, we use a memo to avoid recalculations.
  const percentage = useMemo(
    () => ((progress * circ) / 100),
    [ progress, circ ]
  )

  return (
    <>
      <div
        className="container"
        style={{
          '--margin': `${margin}px`,
          '--size': `${size}px`,
          '--radius': radius,
          '--circ': circ,
          '--percentage': percentage,
          '--stroke-size': stroke,
          '--stroke-color': `var(--appico-accent-main-color-${accentColor})`,
          '--background-color': `var(--appico-accent-seco-color-${accentColor})`,
        }}
      >
        <svg viewBox={`-${margin} -${margin} ${size} ${size}`}>
          <circle className="circle-base" />
          <circle className="circle-arc" />
        </svg>
        <div className="circle-paper" />
      </div>
      <style jsx>
        {`
          @keyframes load-animation {
            from {
              stroke-dasharray: 0 var(--circ);
            }
          }

          .container {
            width: var(--size);
            height: var(--size);
            transform: rotate(-90deg);
            position: relative;
          }

          .container > svg {
            margin: 0px;
            background-color: var(--background-color);
            border-radius: calc(var(--size) / 2);
          }

          .circle-base {
            fill: var(--background-color);
            r: var(--radius);
            cx: var(--radius);
            cy: var(--radius);
          }

          .circle-arc {
            fill: none;
            stroke: var(--stroke-color);
            stroke-linecap: round;
            stroke-dasharray: var(--percentage), var(--circ);
            stroke-width: var(--stroke-size);
            r: var(--radius);
            cx: var(--radius);
            cy: var(--radius);
            animation: load-animation 1.5s ease forwards;
            animation-delay: 250ms;
          }

          .circle-paper {
            --circle-paper-size: calc(var(--size) - (var(--margin) * 4));
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            margin: auto;
            width: var(--circle-paper-size);
            height: var(--circle-paper-size);
            border-radius: calc(var(--circle-paper-size) / 2);
            background-color: var(--appico-paper-color);
          }
        `}
      </style>
    </>
  )
}

PieChart.defaultProps = {
  progress: 30,
  size: 110,
  margin: 5,
  stroke: 8,
  accentColor: 1,
}

PieChart.propTypes = {
  /** Progress percentage, from 0 to 100 */
  progress: PropTypes.number,
  /** Size of the graph */
  size: PropTypes.number,
  /** Margin of the graph */
  margin: PropTypes.number,
  /** Strokes width */
  stroke: PropTypes.number,
  /** Accent color code, 1 or 2 */
  accentColor: PropTypes.oneOf([1, 2]),
}

export default PieChart
