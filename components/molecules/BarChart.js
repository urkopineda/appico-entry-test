import React from 'react'
import PropTypes from 'proptypes'

/**
 * This component renders a bar chart.
 * @param {object} props check the props doc in the PropTypes definition bellow.
 */
const BarChart = ({ data, maxWidth, maxHeight, accentColor }) => (
  <>
    <div
      className="container"
      style={{
        '--max-width': `${maxWidth}px`,
        '--max-height': `${maxHeight}px`,
      }}
    >
      {data.map((d, i) => (
        <div
          key={i}
          className="progress-container"
          style={{
            '--progress': `${d.progress}%`,
            '--background-color': `var(--appico-accent-${((i % 2) === 0) ? 'main' : 'seco'}-color-${accentColor})`,
          }}
        >
          <div className="progress-subcontainer">
            <div className="progress" />
          </div>
          <span>
            {d.label}
          </span>
        </div>
      ))}
    </div>
    <style jsx>
      {`
        @keyframes load-animation {
          from {
            max-height: 0px;
          }
        }

        .container {
          width: 100%;
          height: var(--max-height);
          max-width: var(--max-width);
          display: flex;
          flex-direction: row;
          justify-content: space-around;
          align-items: center;
        }

        .progress-container {
          height: 100%;
          display: flex;
          flex-direction: column;
          align-items: center;
        }

        .progress-subcontainer {
          flex: 1;
          width: 10px;
          display: flex;
          flex-direction: column;
          justify-content: flex-end;
          align-items: center;
        }

        .progress {
          width: 100%;
          height: 100%;
          border-radius: 2px;
          background-color: var(--background-color);
          max-height: var(--progress);
          animation: load-animation 1.5s ease forwards;
          animation-delay: 250ms;
        }

        .progress-container > span {
          margin: 10px 0px 0px 0px;
          color: var(--appico-text-disabled-color);
          font-size: 12px;
        }
      `}
    </style>
  </>
)

BarChart.defaultProps = {
  data: [
    {
      label: 'a',
      progress: 70,
    },
    {
      label: 'b',
      progress: 20,
    },
    {
      label: 'c',
      progress: 60,
    },
  ],
  maxWidth: 200,
  maxHeight: 75,
  accentColor: 1,
}

BarChart.propTypes = {
  /** Data array with @label and @progress */
  data: PropTypes.arrayOf(
    /** Data element with @label and @progress */
    PropTypes.shape({
      /** Label to be shown on the chart */
      label: PropTypes.string.isRequired,
      /** Progress percentage, from 0 to 100 */
      progress: PropTypes.number.isRequired,
    })
  ),
  /** Max width of the graph */
  maxWidth: PropTypes.number,
  /** Max height of the graph */
  maxHeight: PropTypes.number,
  /** Accent color code, 1 or 2 */
  accentColor: PropTypes.oneOf([1, 2]),
}

export default BarChart
