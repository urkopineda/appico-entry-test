import React from 'react'
import Head from 'next/head'

import NavbarTop from './organisms/NavbarTop'
import NavbarSide from './organisms/NavbarSide'

/**
 * We use this component to define the HTML structure and for adding some static metadata
 * to the pages using `next/head`.
 * @param {object} props we only use children to render the current page.
 */
const Layout = (props) => {
  const {
    children,
  } = props

  return (
    <>
      <Head>
        <link
          rel="shortcut icon"
          href="/favicon.ico"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="192x192"
          href="/meta/android-chrome-192x192.png"
        />
        <link
          rel="manifest"
          href="/site.webmanifest"
        />
        <meta
          name="application-name"
          content="Appico - Urko Pineda Olmo"
        />
        <meta
          name="description"
          content="Appico Dashboard UI exercise by Urko Pineda Olmo"
        />
        <meta
          name="theme-color"
          content="#0077FF"
        />
        <meta
          name="apple-mobile-web-app-capable"
          content="yes"
        />
        <meta
          name="apple-mobile-web-app-title"
          content="Appico - Urko"
        />
        <meta
          name="apple-mobile-web-app-status-bar-style"
          content="default"
        />
        <link
          rel="apple-touch-icon"
          href="/apple-touch-icon.png?v=1"
        />
        <link
          rel="apple-touch-icon"
          sizes="57x57"
          href="/apple-touch-icon-57x57.png?v=1"
        />
        <link
          rel="apple-touch-icon"
          sizes="60x60"
          href="/apple-touch-icon-60x60.png?v=1"
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href="/apple-touch-icon-72x72.png?v=1"
        />
        <link
          rel="apple-touch-icon"
          sizes="76x76"
          href="/apple-touch-icon-76x76.png?v=1"
        />
        <link
          rel="apple-touch-icon-precomposed"
          href="/apple-touch-icon-precomposed.png?v=1"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="57x57"
          href="/apple-touch-icon-57x57-precomposed.png?v=1"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="60x60"
          href="/apple-touch-icon-60x60-precomposed.png?v=1"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="72x72"
          href="/apple-touch-icon-72x72-precomposed.png?v=1"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="76x76"
          href="/apple-touch-icon-76x76-precomposed.png?v=1"
        />
        <link
          rel="mask-icon"
          href="/safari-pinned-tab.svg?v=1"
          color="#000000"
        />
        <meta
          name="msapplication-TileColor"
          content="#000000"
        />
        <meta
          name="msapplication-navbutton-color"
          content="#000000"
        />
        <meta
          name="msapplication-starturl"
          content="/"
        />
        <meta
          name="msapplication-TileImage"
          content="/mstile-144x144.png?v=1"
        />
        <meta
          name="msapplication-config"
          content="/browserconfig.xml?v=1"
        />
      </Head>
      <div className="container">
        <NavbarTop />
        <div className="subcontainer">
          <NavbarSide />
          <main>
            {children}
          </main>
        </div>
      </div>
      <style jsx>
        {`
          @media (min-width: 520px) {
            main {
              padding: 30px 40px;
            }
          }

          @media (max-width: 520px) {
            main {
              padding: 20px 15px;
            }
          }

          .container {
            width: 100%;
            height: 100%;
            display: flex;
            flex-direction: column;
          }
          
          .subcontainer {
            flex: 1;
            display: flex;
            flex-direction: row;
          }

          main {
            flex: 1;
            max-width: calc(100vw - var(--appico-navbar-side-size));
          }
        `}
      </style>
      <style jsx global>
        {`
          :root {
            --appico-accent-main-color-1: #0077FF;
            --appico-accent-main-color-2: #F0166D;
            --appico-accent-seco-color-1: #B2D6FF;
            --appico-accent-seco-color-2: #FAB9D3;
            --appico-accent-font-color: #FFFFFF;
            --appico-button-color: #0068DE;
            --appico-logo-size: 36px;
            --appico-button-min-width: 89px;
            --appico-button-min-height: 35px;
            --appico-button-border-radius: 4px;
            --appico-paper-border-radius: 3.8px;
            --appico-icon-size: 23px;
          }
          
          @media screen and (prefers-color-scheme: light) {
            :root {
              --appico-text-enabled-color: #0068DE;
              --appico-text-disabled-color: #6F6F6F;
              --appico-text-disabled-lighter-color: #6F6F6F;
              --appico-background-color: #FAFBFC;
              --appico-divider-color: #D8D8D8;
              --appico-paper-color: #FFFFFF;
              --appico-linechart-color: #C7CDD9;
              --appico-font-color: #000000;
            }
          }
          
          @media screen and (prefers-color-scheme: dark) {
            :root {
              --appico-text-enabled-color: #4096FF;
              --appico-text-disabled-color: #818181;
              --appico-text-disabled-lighter-color: #A9A9A9;
              --appico-background-color: #1E1E1E;
              --appico-divider-color: #2B2A2A;
              --appico-paper-color: #000000;
              --appico-linechart-color: #C7CDD9;
              --appico-font-color: #FFFFFF;
            }
          }

          @media (min-width: 560px) {
            :root {
              --appico-navbar-side-size: 90px;
            }
          }

          @media (max-width: 560px) {
            :root {
              --appico-navbar-side-size: 40px;
            }
          }

          @media (min-width: 520px) {
            :root {
              --appico-navbar-top-size: 70px;
            }
          }

          @media (max-width: 520px) {
            :root {
              --appico-navbar-top-size: 77px;
            }
          }
          
          @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: local('Lato Regular'),
                 local('Lato-Regular'),
                 url('/lato.woff2') format('woff2'),
                 url('/lato.woff')  format('woff');
          }
          
          html {
            width: 100vw;
            height: 100vh;
            font-family: 'Lato', sans-serif;
            background-color: var(--appico-background-color);
            color: var(--appico-font-color);
          }
          
          body {
            margin: 0px;
          }
          
          body, #__next {
            width: 100%;
            height: 100%;
          }
          
          * {
            box-sizing: border-box;
          }
          
          a {
            color: inherit;
            text-decoration: none;
          }

          h1 {
            margin: 0px 0px 20px 0px;
            font-size: 22px;
            font-weight: normal;
          }

          h2 {
            margin: 0px 0px 10px 0px;
            font-size: 19px;
            font-weight: normal;
          }

          p {
            margin: 0px 0px 10px 0px;
            font-size: 17px;
            font-weight: normal;
          }
        `}
      </style>
    </>
  )
}

export default Layout
