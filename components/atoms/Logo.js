import React from 'react'

/**
 * This component renders a logo, no props needed.
 */
const Logo = () => (
  <>
    <div className="container">
      .
    </div>
    <style jsx>
      {`
        @media (hover: hover) and (pointer: fine) {
          .container:hover {
            background-color: var(--appico-accent-seco-color-1);
          }
        }
        
        .container {
          width: var(--appico-logo-size);
          height: var(--appico-logo-size);
          border-radius: calc(var(--appico-logo-size) / 2);
          background-color: var(--appico-accent-main-color-1);
          color: var(--appico-accent-font-color);
          font-size: 25px;
          order: 0;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: flex-end;
          user-select: none;
          cursor: pointer;
          transition: 0.2s;
        }
      `}
    </style>
  </>
)

export default Logo
