import React from 'react'
import PropTypes from 'proptypes'

/**
 * This component renders a simple button.
 * @param {object} props check the props doc in the PropTypes definition bellow.
 */
const Button = ({ children, disabled, onClick }) => (
  <>
    <button
      data-disabled={(disabled) ? '' : undefined}
      onClick={onClick}
    >
      {children}
    </button>
    <style jsx>
      {`
        @media (hover: hover) and (pointer: fine) {
          button:hover {
            transform: scale(0.95);
          }
        
          button:active {
            transform: scale(0.9);
          }
        }

        button:focus {
          outline: 0;
        }

        button {
          min-width: var(--appico-button-min-width);
          min-height: var(--appico-button-min-height);
          border-radius: var(--appico-button-border-radius);
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          user-select: none;
          cursor: pointer;
          transition: 0.2s;
          font-size: 14px;
          min-width: 60px;
        }

        button:not([data-disabled]) {
          background-color: var(--appico-button-color);
          color: var(--appico-accent-font-color);
        }

        button[data-disabled] {
          border-style: solid;
          border-width: 1px;
          border-color: var(--appico-text-disabled-lighter-color);
          background-color: var(--appico-background-color);
          color: var(--appico-text-disabled-lighter-color);
        }
      `}
    </style>
  </>
)

Button.defaultProps = {
  children: 'Default',
  disabled: false,
}

Button.propTypes = {
  /** Text to view inside the button, could be anything can be rendered */
  children: PropTypes.any.isRequired,
  /** Button disabled or not */
  disabled: PropTypes.bool,
  /** On click event */
  onClick: PropTypes.func,
}

export default Button
