import React from 'react'
import PropTypes from 'prop-types'

/**
 * This component uses the SVG sprite in `public/icons.svg` to render icons.
 * @param {object} props check the props doc in the PropTypes definition bellow.
 */
const Icon = (props) => {
  const {
    active,
    children: icon,
    onClick,
  } = props

  return (
    <>
      <div
        className="container"
        data-active={(active) ? '' : undefined}
        onClick={onClick}
      >
        <svg>
          <use href={`/icons.svg#${icon}`} />
        </svg>
      </div>
      <style jsx>
        {`
          .container {
            width: var(--appico-icon-size);
            height: var(--appico-icon-size);
            display: flex;
            align-items: center;
            justify-content: center;
          }
          
          .container[data-active] {
            fill: var(--appico-text-enabled-color);
          }
          
          .container:not([data-active]) {
            fill: var(--appico-text-disabled-color);
          }
          
          .container > svg {
            width: inherit;
            height: inherit;
          }
        `}
      </style>
    </>
  )
}

Icon.defaultProps = {
  active: true,
  children: undefined,
  onClick: () => {}
}

Icon.propTypes = {
  /** Set if icon is active or not, default `true` */
  active: PropTypes.bool,
  /** Icon code, check `public/icons` */
  children: PropTypes.string.isRequired,
  /** Function to be executed on click event */
  onClick: PropTypes.func,
}

export default Icon
