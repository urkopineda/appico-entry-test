import React from 'react'
import PropTypes from 'proptypes'

/**
 * This component is a paper, a concept taken from Material Design to render simple
 * pieces of paper on the UI.
 * @param {object} props check the props doc in the PropTypes definition bellow.
 */
const Paper = ({ padding, borders, bradius, children }) => (
  <>
    <div
      className="container"
      data-add-padding={(padding) ? '' : undefined}
      data-border-top={(borders.top) ? '' : undefined}
      data-border-bottom={(borders.bottom) ? '' : undefined}
      data-border-left={(borders.left) ? '' : undefined}
      data-border-right={(borders.right) ? '' : undefined}
      data-add-bradius={(bradius) ? '' : undefined}
    >
      {children}
    </div>
    <style jsx>
      {`
        @media (min-width: 560px) {
          .container[data-add-padding] {
            padding: 30px;
          }
        }

        @media (max-width: 560px) {
          .container[data-add-padding] {
            padding: 30px 20px;
          }
        }

        .container {
          background-color: var(--appico-paper-color);
          border-width: 1px;
          border-color: var(--appico-divider-color);
          overflow: hidden;
        }

        .container[data-border-top] {
          border-top-style: solid;
        }

        .container[data-border-bottom] {
          border-bottom-style: solid;
        }

        .container[data-border-left] {
          border-left-style: solid;
        }

        .container[data-border-right] {
          border-right-style: solid;
        }

        .container[data-add-bradius] {
          border-radius: var(--appico-paper-border-radius);
        }
      `}
    </style>
  </>
)

Paper.defaultProps = {
  padding: true,
  borders: {
    top: true,
    bottom: true,
    left: true,
    right: true,
  },
  bradius: true,
}

Paper.propTypes = {
  /** Add paddings or not */
  padding: PropTypes.bool,
  /** Borders set up (top, bottom, left, right) */
  borders: PropTypes.shape({
    top: PropTypes.bool,
    bottom: PropTypes.bool,
    left: PropTypes.bool,
    right: PropTypes.bool,
  }),
  /** Add border radius or not */
  bradius: PropTypes.bool,
}

export default Paper
