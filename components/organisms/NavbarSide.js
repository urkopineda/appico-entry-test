import React, { useEffect, useState, useMemo } from 'react'
import Link from 'next/link'
import {
  Router,
  useRouter,
} from 'next/router'

import Icon from '../atoms/Icon'

// Here we obtain the menu data to be rendered.
import menuData from '../../data/menu.js'

/**
 * Get the active menu to be highlighted.
 * @param {object} router current router state.
 */
const getActiveMenu = (router) => menuData.find((menu) => menu.href === router.pathname)

/**
 * Get the active submenu to be highlighted.
 * @param {object} router current router state.
 */
const getActiveSubmenu = (router) => {
  const activeMenu = getActiveMenu(router)
  if (activeMenu && activeMenu.submenu) {
    let activeSubmenu = activeMenu.submenu[0]
    if (Object.keys(router.query).length) {
      activeSubmenu = activeMenu.submenu.find((submenu) =>
        submenu.href.query && submenu.href.query.section === router.query.section
      )
    }
    return (activeSubmenu || activeMenu.submenu[0]).key
  }
}

/**
 * Extract data from router.
 * @param {string} asPath full path of the web app, including GET params.
 */
const getRouterData = ({ asPath }) => {
  const regex = /(\/[\w]*)(?:\?section=([\w]+))?/.exec(asPath)
  return {
    pathname: regex[1],
    query: {
      section: regex[2],
    },
  }
}

/**
 * Render the navbar side component.
 */
const NavbarSide = () => {
  const router = useRouter()
  // Get router data, we use a memo to avoid recalculations.
  const routerData = useMemo(
    () => getRouterData(router),
    [ router ],
  )
  // State of the active menu.
  const [ activeMenu, setActiveMenu ] = useState(getActiveMenu(routerData))
  // State of the active submenu.
  const [ activeSubmenu, setActiveSubmenu ] = useState()

  // Define the router change listeners and set the submenu state
  // We set it on mount because GET parameters are not shown by the router until mounted
  // PS: Could be a Next.js router bug.
  useEffect(() => {
    setActiveSubmenu(getActiveSubmenu(routerData))
    const handle = (url) => {
      const r = getRouterData({
        asPath: url,
      })
      setActiveMenu(getActiveMenu(r))
      setActiveSubmenu(getActiveSubmenu(r))
    }
    Router.events.on('routeChangeComplete', handle)
    return () => {
      Router.events.off('routeChangeComplete', handle)
    }
  })

  return (
    <>
      <div className="container">
        {((activeMenu && activeMenu.submenu) || []).map(({ key, href, icon }) => (
          <div
            key={key}
            className="submenu-element-container"
            data-active={(activeSubmenu === key) ? '' : undefined}
          >
            <Link href={href}>
              <a aria-label={key}>
                <div className="submenu-element-icon">
                  <Icon
                    active={(activeSubmenu === key)}
                  >
                    {icon}
                  </Icon>
                </div>
                <div className="submenu-element-indicator" />
              </a>
            </Link>
          </div>
        ))}
      </div>
      <style jsx>
        {`
          @media (hover: hover) and (pointer: fine) {
            .submenu-element-container:hover .submenu-element-indicator {
              background-color: var(--appico-accent-main-color-1);
            }
          }
          
          .container {
            width: var(--appico-navbar-side-size);
            background-color: var(--appico-paper-color);
            border-color: var(--appico-divider-color);
            border-width: 0px 0.1px 0.1px 0px;
            border-style: solid;
            display: flex;
            flex-direction: column;
            padding: 20px 0px;
            transition: 0.2s;
          }
          
          .submenu-element-container {
            width: 100%;
            height: 50px;
            margin: 10px 0px;
            user-select: none;
          }
          
          .submenu-element-container a {
            height: 100%;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
          }

          .submenu-element-icon {
            padding: 0px 5px;
            margin-left: auto;
          }
          
          .submenu-element-indicator {
            height: 100%;
            width: 2px;
            margin-left: auto;
            transition: 0.2s;
          }
          
          .submenu-element-container[data-active] .submenu-element-indicator {
            background-color: var(--appico-accent-main-color-1);
          }

        `}
      </style>
    </>
  )
}

export default NavbarSide
