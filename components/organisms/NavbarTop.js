import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import {
  Router,
  useRouter,
} from 'next/router'

import Logo from '../atoms/Logo'
import Button from '../atoms/Button'

// Here we obtain the menu data to be rendered.
import menuData from '../../data/menu.js'

/**
 * Get the active menu to be highlighted.
 * @param {object} router current router state.
 */
const getActiveMenu = (router) => (menuData.find((menu) => menu.href === router.pathname) || {})

/**
 * Get the active menu key.
 * @param {object} router current router state.
 */
const getActiveMenuKey = (router) => getActiveMenu(router).key

/**
 * Render the top navbar component.
 */
const NavbarTop = () => {
  const router = useRouter()
  // State of the active menu.
  const [ activeMenu, setActiveMenu ] = useState(getActiveMenuKey(router))

  // Define the router change listeners.
  useEffect(() => {
    const handle = (url) => {
      const regex = /(\/[\w]*)(?:\?[\w]+\=[\w]+)?/.exec(url)
      setActiveMenu(
        getActiveMenuKey({
          pathname: regex[1],
        })
      )
    }
    Router.events.on('routeChangeComplete', handle)
    return () => {
      Router.events.off('routeChangeComplete', handle)
    }
  })

  return (
    <>
      <Head>
        <title>
          {getActiveMenu(router).text} - Appico
        </title>
      </Head>
      <div className="container">
        <Link href="/">
          <a>
            <Logo />
          </a>
        </Link>
        <div className="menu-container">
          {menuData.map(({ key, href, text }) => (
            <div
              key={key}
              className="menu-element-container"
              data-active={(activeMenu === key) ? '' : undefined}
            >
              <Link href={href}>
                <a>
                  <span>
                    {text}
                  </span>
                  <div className="menu-element-indicator" />
                </a>
              </Link>
            </div>
          ))}
        </div>
        <div className="button-container">
          <Button>
            Premium
          </Button>
        </div>
      </div>
      <style jsx>
        {`
          @media (hover: hover) and (pointer: fine) {
            .menu-element-container:hover .menu-element-indicator {
              background-color: var(--appico-accent-main-color-1);
            }
          
            .menu-element-container:hover {
              color: var(--appico-accent-main-color-1);
            }
          
            .menu-element-container:not(:hover):not([data-active]) {
              color: var(--appico-text-disabled-color);
            }
          }

          @media (min-width: 520px) {
            .menu-container {
              height: 100%;
              padding: 0px 5px;
            }
          }

          @media (max-width: 520px) {
            .menu-container {
              min-height: 30px;
              min-width: 100%;
              order: 2;
              padding: 10px 0px 0px 0px;
            }
          }
          
          .container {
            width: 100vw;
            height: var(--appico-navbar-top-size);
            padding: 5px 25px 0px 25px;
            background-color: var(--appico-paper-color);
            border-color: var(--appico-divider-color);
            border-width: 0px 0px 0.1px 0px;
            border-style: solid;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            align-items: center;
            transition: 0.2s;
          }

          .container > a {
            margin-right: auto;
          }
          
          .menu-container {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
          }
          
          .menu-element-container {
            height: 100%;
            margin: 0px 20px;
            user-select: none;
          }
          
          .menu-element-container[data-active] {
            color: var(--appico-text-enabled-color);
          }
          
          .menu-element-container a {
            height: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
          }
          
          .menu-element-container span {
            margin-top: auto;
            transition: 0.2s;
            padding: 0px 0px 5px 0px;
          }
          
          .menu-element-indicator {
            width: 100%;
            height: 2px;
            margin-top: auto;
            transition: 0.2s;
          }
          
          .menu-element-container[data-active] .menu-element-indicator {
            background-color: var(--appico-accent-main-color-1);
          }

          .button-container {
            margin-left: auto;
          }
        `}
      </style>
    </>
  )
}

export default NavbarTop
